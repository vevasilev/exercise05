import java.util.*;

public class Exercise05 {
    public static void main(String[] args) {
        String text = "Object-oriented programming is a programming language model organized around objects rather than \"actions\" and data rather than logic. Object-oriented programming blabla. Object-oriented programming bla.";
        String originalSubstring = "Object-oriented programming";
        String substringToReplace = "OOP";

        System.out.println(text);
        text = replacingSubstrings(text, originalSubstring, substringToReplace);
        System.out.println(text);

        System.out.println();

        String test = "fffff ab f 1234 jkjk";
        System.out.println(test);
        System.out.println("A word in which the number of unique symbols is minimal: " + getWord(test));
        System.out.println("The number of words containing only Latin alphabet symbols: " + getCountWords(test));
        getWordsPalindromes(test);
    }

    private static void getWordsPalindromes(String strTest) {
        String[] words = strTest.split("\\s+");
        StringBuilder builder = new StringBuilder();
        if (words.length != 0) {
            HashSet<String> palindromes = new HashSet<>();
            for (String word : words) {
                var reverse = builder.append(word).reverse().toString();
                if (reverse.equalsIgnoreCase(word)) {
                    palindromes.add(word);
                }
                builder.setLength(0);
            }
            System.out.println(!palindromes.isEmpty() ? "Palindrome words: " + palindromes : "The words palindromes are missing in the string.");
        }
    }

    private static int getCountWords(String strTest) {
        String[] words = strTest.split("\\s+");
        int countWordContainOnlyLatinSymbols = 0;
        for (String word : words) {
            boolean onlyLatinSymbols = true;
            for (int i = 0; i < word.length(); i++) {
                char symbol = word.charAt(i);
                if ((Character.toUpperCase(symbol) < 65 || Character.toUpperCase(symbol) > 90)) {
                    onlyLatinSymbols = false;
                }
            }
            if (onlyLatinSymbols) {
                countWordContainOnlyLatinSymbols++;
            }
        }
        return countWordContainOnlyLatinSymbols;
    }

    private static String getWord(String strTest) {
        String[] words = strTest.split("\\s+");
        if (words.length == 0) {
            return "";
        } else {
            var indexMin = (words[0].equals("") && words.length != 1) ? 1 : 0;
            var countMin = getCountUniqueSymbols(words[indexMin].trim());
            for (int index = 1; index < words.length; index++) {
                var symbols = getCountUniqueSymbols(words[index].trim());
                if (countMin > symbols) {
                    indexMin = index;
                    countMin = symbols;
                }
            }
            return words[indexMin];
        }
    }

    private static int getCountUniqueSymbols(String word) {
        HashSet<Character> symbols = new HashSet<>();
        for (int i = 0; i < word.length(); i++) {
            symbols.add(word.charAt(i));
        }
        return symbols.size();
    }

    private static String replacingSubstrings(String text, String originalSubstring, String substringToReplace) {
        int count = 1;
        StringBuilder builder = new StringBuilder();
        while (text.toLowerCase().contains(originalSubstring.toLowerCase())) {
            int index = text.toLowerCase().indexOf(originalSubstring.toLowerCase());
            builder.append(text, 0, index).append(count % 2 == 0 ? substringToReplace : text.substring(index, index + originalSubstring.length()));
            text = text.substring(index + originalSubstring.length());
            count++;
        }
        if (!text.isEmpty()) {
            builder.append(text);
        }
        return builder.toString();
    }
}